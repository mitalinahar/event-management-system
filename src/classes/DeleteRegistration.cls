global class DeleteRegistration implements Database.Batchable<sObject>
{
    global final String Query;
    global deleteRegistration(String q)
    {
        Query=q;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        List <Registration__C> lstReg = new list<Registration__C>();
        for(Sobject objS : scope)
        {
            Registration__C objReg = (Registration__C)objS;
            lstReg.add(objReg);
        }
        Delete lstReg;
    }
    
    global void finish(Database.BatchableContext BC)
    {
        //Send an email to the User after your batch completes
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {'mitali@concret.io'};
            mail.setToAddresses(toAddresses);
        mail.setSubject('Apex Batch Job is done');
        mail.setPlainTextBody('The batch Apex job processed ');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}