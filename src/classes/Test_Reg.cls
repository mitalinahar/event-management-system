@isTest
public class Test_Reg {
    static testMethod void testRegistration(){
        
        Registration_Ctrllr reg = new Registration_Ctrllr();
        reg.conFname =  'Robert';
        reg.conLname = 'Frost';
        reg.conEmail = 'robert@gmail.com';
        DateTime startT = datetime.newInstance(2016,5,21,10,00,00);
        DateTime endT = datetime.newInstance(2016,5,21,11,00,00);
        Venue__c myVenue = new Venue__C (name='venue1' , city__C='Delhi');
        insert myVenue;
        Event__c objEvent = new Event__c(name='testEvent1',Event_Date__C=Date.Today().addDays(2),Registration_Deadline__c=Date.Today().addDays(1),Total_Capacity__c=50,Venue__c=myVenue.id);
        insert objEvent;
        reg.selectedEvent = objEvent.id;
        Session__c objSession =new Session__c(name='Session1', Event__c = objEvent.id , capacity__c=10 , Start_Time__c=startT , End_Time__c=endT);
        insert objSession;
        reg.selectedSession = objSession.id;
        system.debug('Event'+objEvent.Event_Date__C);
        system.debug('Session'+startT);
        List<SelectOption> optList = reg.optionsEvent;
        
        test.startTest();
        reg.sessionList();
        reg.doRegister();
        reg.basicCallout();
        test.stoptest();
        //system.assertEquals(,);
    }
}