public class Registration_Ctrllr {
    public List<MyWrapper> myWrapperList {get;set;}
    public String result{get;set;}
    public Boolean panel {get;set;}
    public Boolean block {get;set;}
    public String conFname{get;set;} 
    public String conLname{get;set;}
    public String conEmail{get;set;}
    public String selectedEvent{get;set;}
    public String selectedSession{get;set;}
    public List<SelectOption> optionsSession{get;set;}
    public boolean allComplete {get;set;}
    public String address{get;set;}
     public String city{get;set;}
     public String phone{get;set;}
    public boolean show{get;set;}
    
    public Registration_Ctrllr(){
        show = false;
        block = true; 
        panel = false;
        myWrapperList = new List<MyWrapper>();
        allComplete = false;
    }
    
    public Class MyWrapper{
        Public String url{get;set;}
        public String city{get;set;}
        //  public String saving{get;set;}
    }
    
   /* public List<Event__C> lstEvent {
        get{
            // Date newDate = Date.Today();
            return [Select Id , Name from Event__c where Registration_Deadline__c < TODAY];
        }set;
    } */
    
    public List<SelectOption> optionsEvent {
        get{
            optionsEvent = new List<SelectOption>();
            optionsEvent.add(new SelectOption(' ','None'));
            for(Event__C objEve : [Select Id , Name , Event_Date__C from Event__c where Registration_Deadline__c > TODAY]){
                system.debug('obj'+ objEve); 
                
                optionsEvent.add(new SelectOption(objEve.Id, objEve.Name + ' ('+ (objEve.Event_Date__c).format() + ')'));
            }
            return optionsEvent ;
        }
        set;
    }
    
    public Void sessionList(){
        if(selectedEvent == ' '){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Warning,'Please choose an event'));
            optionsSession = new List<SelectOption>();
        }
        else{
            optionsSession = new List<SelectOption>();
            optionsSession.add(new SelectOption(' ','None'));
            for(Session__c objSession : [Select Id, Name, Start_Time__c , End_Time__C , Total_Registrations__c , Capacity__c from Session__c where Event__c =: selectedEvent order by Start_Time__c]){
                if(objSession.Total_Registrations__c == null){
                    objSession.Total_Registrations__c = 0 ;
                }
                if(objSession.Total_Registrations__c < objSession.Capacity__c){
                    Integer myStartHour = objSession.Start_Time__c.hour();
                    Integer myEndHour = objSession.End_Time__c.hour();
                    
                    optionsSession.add(new SelectOption(objSession.Id , objSession.Name + ' (  ' + (myStartHour > 12 ? myStartHour - 12 : myStartHour )  + ':' + objSession.Start_Time__c.minute() + (myStartHour > 12 ? ' PM':' AM') +' -- ' + (myEndHour > 12 ? myEndHour - 12 : myEndHour) + ':' +  objSession.End_Time__c.minute() + (myEndHour > 12 ? ' PM':' AM')+ ')'));
                }
            }
        }
        Event__c objEve = [ Select Venue__r.city__C ,Venue__r.address__c,Venue__r.phone__c  from Event__c where id = : selectedEvent];
       address = ((String)objEve.Venue__r.address__c ); 
       city =  ((String)objEve.Venue__r.city__c );
        phone = ((String)objEve.Venue__r.phone__c );
        show = true;
        system.debug('address'+address);
        system.debug('show'+show);
    }
    
    public void clearAll(){
        conFname = '';
        conLname = '';
        conEmail = '';
        selectedEvent = ' ';
        selectedSession = ' ';
        show = false;
    }
    
    
    public void doRegister(){
        system.debug('Register ME');
        system.debug('@#'+ conFname);
        if(String.isBlank(conFname) ){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error,' First Name is Required'));
            return;
        }
        if(String.isBlank(conLname) ){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error,' Last Name is Required'));
            return;
        }
        if(String.isBlank(conEmail)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error,' Email Id is Required'));
            return;
        }
        if(string.isNotBlank(conEmail)){
            Boolean res = true;
            String emailRegex = '^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$';
            Pattern MyPattern = Pattern.compile(emailRegex);
            Matcher MyMatcher = MyPattern.matcher(conEmail);
            
            if (!MyMatcher.matches()) {
                res = false;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Warning,'Please enter a valid Email Address'));
                return;   
            }
        }   
        if(selectedEvent == ' '){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Warning,'Please choose an event'));
            return;
        }
        if(selectedSession == ' '){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Warning,'Please choose a Session'));
            return;
        }
        
        Id conId ;
        List <Contact> lstcon = new List<contact> ([Select Id , Email from contact where Email =: conEmail LIMIT 1]);
        if(lstcon.isEmpty()){
            Contact newCon = new Contact(FirstName = conFname , LastName = conLname , email = conEmail);
            insert newCon;
            conId = newCon.id;
        }
        
        if(!lstcon.isEmpty()){
            conId = lstcon[0].id;
            Contact oldCon = [Select Email , (Select Session__C from Registrations__r where session__c =: selectedSession) from Contact where id =: conId];
            if(oldCon.Registrations__r.Size() > 0){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error,' You cant register for the same Event and Session More than once'));
                return;
            }
        }  

        system.debug('Session Id'+ selectedSession );
        Registration__c newReg = new Registration__c (Contact__c = conId , Event__c = selectedEvent , Session__c = selectedSession );
        insert newReg ;
        
        show = false;
        block = false;
        panel = true; 
        
        /* Sending Email
        Contact con = [Select FirstName ,LastName,Email from Contact where id = : conId];
        // List<Contact> con = new List<Contact>([Select LastName,Email from Contact where id =: IdCon]); 
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.setSubject('Registration Completed'); 
        email.setReplyTo('mitali@concret.io');
        email.setSaveAsActivity(true);
        //email.setHtmlBody('<b>Hello HTML</b>'); 
        Event__c objEvent = [Select Name , Event_Date__c from Event__c where id =: selectedEvent];
        String name = String.valueOf(objEvent.Name);
        String eventDate = String.valueOf(objEvent.Event_Date__c);
        Session__c objSession = [Select Name , Start_Time__c , End_Time__C from Session__c where id =: selectedSession];
        String nameS = String.valueOf(objSession.Name);
        String sTime = String.valueOf(objSession.Start_Time__C.time());
        String eTime = String.valueOf(objSession.End_Time__C.time());
        email.setPlainTextBody('Hi ' + con.FirstName + ' ' + con.LastName +',\n You have successfully been registered for the event ' + '"' + name + eventDate +'"' + ' for the Session ' + '"' +nameS + '"' +'(Starts at '+ sTime + '-' + 'Ends at' + eTime + ')' + '\nClick on following link to see details  \nhttps://ap2.salesforce.com/'+con.Id); 
        email.setToAddresses(new String[]{con.Email,'mitali@concret.io'});
        Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{email}); 
*/        
			system.debug('On Last Line');
        allComplete = true;
        
        //basicCallout();
    }
    
  /*  public void showVenue(){
        Event__c objEve = [ Select Venue__r.city__C ,Venue__r.address__c,Venue__r.phone__c  from Event__c where id = : selectedEvent];
       address = ((String)objEve.Venue__r.address__c ); 
       city =  ((String)objEve.Venue__r.city__c );
        phone = ((String)objEve.Venue__r.phone__c );
        show = true;
    } */
    
  /*  public void justCalling(){
        System.debug('---> calllig callling.....');
    }    */
    public void basicCallout(){
        
        system.debug('Insidebasiccallout' + selectedEvent);
            Event__c objEvent = [Select Venue__r.City__c from Event__C where id =: selectedEvent];
            system.debug('objEvent'+objEvent.Venue__r.City__c);
            String city = String.ValueOf(objEvent.Venue__r.City__c);
            HttpRequest req = new HttpRequest();
            req.setEndpoint('http://api.hotwire.com/v1/deal/hotel?apikey=xvcrfkectc8n77yhaupnc5se&limit=1&dest='+ city);
            req.setMethod('GET');
            
            Http http = new Http();
           HTTPResponse res = new  HTTPResponse();
            System.debug('@#'+res.getBody());
        if(!test.isRunningTest()){
             res = http.send(req);
        }
        else{
         String   expectedResult='<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Hotwire><Errors/><Result><HotelDeal><FoundDate>2016-05-17T08:36:32-07:00</FoundDate><CurrencyCode>USD</CurrencyCode><NightDuration>5.0</NightDuration><EndDate>05/31/2016</EndDate><Headline>Delhi 5 Star Hotel, $48/night</Headline><IsWeekendStay>false</IsWeekendStay><Price>48.0</Price><StartDate>05/26/2016</StartDate><Url>http://www.hotwire.com/hotel/superPage.jsp?encDealHash=MTAwOjI5OTkwOjkzNzAxOjUuMDo0Ny45OTk5OTI6WTpZOlk-&amp;rs=20500&amp;xid=x-103&amp;wid=w-3&amp;rid=r-69820702248&amp;startDate=05/26/2016&amp;endDate=05/31/2016&amp;bid=B311402&amp;sid=S298</Url><City>Delhi</City><CountryCode>IN</CountryCode><NeighborhoodLatitude>28.6271</NeighborhoodLatitude><NeighborhoodLongitude>77.2018</NeighborhoodLongitude><Neighborhood>New Delhi</Neighborhood><NeighborhoodId>93701</NeighborhoodId><SavingsPercentage>20</SavingsPercentage><StarRating>5.0</StarRating><StateCode>IN</StateCode></HotelDeal></Result></Hotwire>';
        res.setBOdy(expectedResult);
            
        }
            result = res.getBody();
        
            System.debug('@@@'+ result);
            xmlParsing(result);
        
    }
    
    public void xmlParsing(String str){
        myWrapperList = new List<MyWrapper>();
        system.debug('I was called');
        system.debug('String is'+str);
        if(String.isNotBlank(str)){
            system.debug('I too was called');
            //for(integer i = 0 ; i<str.length(); i++){
            XmlStreamReader reader = new XmlStreamReader(str);
            boolean flag = true;
            while(flag) {
                system.debug('GetEventType'+ reader.getEventType());
                if (reader.getEventType() == XmlTag.START_ELEMENT) {
                    system.debug('StartElementFound'+ reader.getLocalName());
                    if ('HotelDeal' == reader.getLocalName()) {
                        MyWrapper objWrapper =  doThis(reader);
                        system.debug('Iwasreturned'+ objWrapper);
                        myWrapperList.add(objWrapper);
                    }
                    
                }
                if (reader.hasNext()) {
                    reader.next();
                } 
                else {
                    flag = false;
                    break;
                }
            }
            
            // }   
        }
        system.debug('myWrapperList'+ myWrapperList);
    }
    public MyWrapper doThis(XmlStreamReader reader){
        MyWrapper order = new MyWrapper();
        boolean flag = true;
        while(flag) {
            System.debug('@# Tag Name: '+reader.getLocalName());
            
            if (reader.getEventType() == XmlTag.END_ELEMENT) {
                if (reader.getLocalName() == 'HotelDeal') {
                    break;
                }
            } else if (reader.getEventType() == XmlTag.START_ELEMENT) {
                if (reader.getLocalName() == 'Url') {
                    reader.next();
                    if (reader.getEventType() == XmlTag.CHARACTERS) {
                        order.url = reader.getText();
                    }
                } else if (reader.getLocalName() == 'City') {
                    reader.next();
                    if (reader.getEventType() == XmlTag.CHARACTERS) {
                        order.city = reader.getText();
                    }
                }
                /*   else if(reader.getLocalName() == 'SavingsPercentage'){
order.saving =  reader.getText();
} */
            }
            if (reader.hasNext()) {
                system.debug('imMovingitNext');
                reader.next();
            } else {
                flag = false;
                break;
            }
        }
        system.debug('return Object'+ order);
        return order;
    } 
}