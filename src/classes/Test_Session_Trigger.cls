@isTest
public class Test_Session_Trigger {
    
    @TestSetup
    static void TestData(){
        Venue__c myVenue = new Venue__C (name='venue1' , city__C='Delhi');
        insert myVenue;
        Event__c objEvent = new Event__C (name='testEvent1',Event_Date__C=Date.Today().addDays(3),Registration_Deadline__c=Date.Today().addDays(1),Total_Capacity__c=50,Venue__c=myVenue.id);
        insert objEvent;
        Date myDate = Date.today().addDays(3);
        integer year = myDate.year();
        integer month = myDate.month();
        integer day = myDate.day();
        DateTime startT = datetime.newInstance(year,month,day,10,00,00);
        DateTime endT = datetime.newInstance(year,month,day,11,00,00);
        DateTime startTime = startT.addHours(2);
        DateTime endTime = startT.addHours(3);
        List<Session__c> sessionList = new List<Session__C>();
        sessionList.add(new Session__c (name='Session1', Event__c = objEvent.id , capacity__c=10 , Start_Time__c=startT,End_Time__c=endT));
        sessionList.add(new Session__c (name='Session2', Event__c = objEvent.id , capacity__c=10 , Start_Time__c=startTime,End_Time__c=endTime));
        insert sessionList;
    }
    
    static testMethod void testBeforeInsert(){
        Date myDate = Date.today().addDays(3);
        integer year = myDate.year();
        integer month = myDate.month();
        integer day = myDate.day();
        DateTime startT = datetime.newInstance(year,month,day,01,00,00);
        DateTime endT = datetime.newInstance(year,month,day,02,00,00);
         DateTime startTime = startT.addHours(2);
        DateTime endTime = startT.addHours(3);
        Event__c objEvent = [Select name from Event__c where name='testEvent1'];  
        List<Session__c> lstSession = new List<Session__c> ();
        lstSession.add(new Session__c(name='Session3', Event__c = objEvent.id , capacity__c=10 , Start_Time__c=startT,End_Time__c=endT)); 
         lstSession.add(new Session__c(name='Session4', Event__c = objEvent.id , capacity__c=10 , Start_Time__c=startTime,End_Time__c=endTime)); 
        
        test.startTest();
        insert lstSession;
        test.stopTest();
        system.assertEquals(2, lstSession.size());
    } 
    
    static testMethod void testBeforeUpdate(){
        Date myDate = Date.today().addDays(3);
        integer year = myDate.year();
        integer month = myDate.month();
        integer day = myDate.day();
        DateTime startT = datetime.newInstance(year,month,day,01,00,00);
        DateTime endT = datetime.newInstance(year,month,day,02,00,00);
        Session__C objSession = [Select Start_Time__c , capacity__c , End_Time__c from Session__c where name='Session1'];
        objSession.Capacity__c = 20;
        objSession.start_Time__c = startT;
        test.startTest();
        update objSession;
        test.stopTest();
        system.assertEquals(20, objSession.Capacity__c);
    } 
}