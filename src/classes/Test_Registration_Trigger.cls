@isTest
public class Test_Registration_Trigger {
    
    @TestSetup
    static void TestData(){
        Venue__c myVenue = new Venue__C (name='venue1' , city__C='Delhi');
        insert myVenue;
        Event__c objEvent = new Event__c(name='testEvent1',Event_Date__C=Date.Today().addDays(3),Registration_Deadline__c=Date.Today().addDays(1),Total_Capacity__c=50,Venue__c=myVenue.id);
        insert objEvent;
        Date myDate = Date.today().addDays(3);
        integer year = myDate.year();
        integer month = myDate.month();
        integer day = myDate.day();
        DateTime startT = datetime.newInstance(year,month,day,10,00,00);
        DateTime endT = datetime.newInstance(year,month,day,11,00,00);
        DateTime startTime = startT.addHours(2);
        DateTime endTime = startT.addHours(3);
        Session__c objSession = new Session__c(name='Session1', Event__c = objEvent.id , capacity__c=10 , Start_Time__c=startT,End_Time__c=endT,Total_Registrations__c=0);
        insert  objSession;
        Session__c objSession1 = new Session__c(name='Session2', Event__c = objEvent.id , capacity__c=10 , Start_Time__c=startTime,End_Time__c=endTime,Total_Registrations__c=0);
        insert  objSession1;
        
        Contact con = new Contact(firstname='hello',lastname='world',email='hello@world.com');
        insert con;
        
        
    }
    
    static testMethod void testAfterInsert(){
        Event__c eve = [Select name from Event__c where name='testEvent1'];
        Session__c sess = [Select name , Total_Registrations__C from Session__C where name='Session1'];
        Contact con = [Select name from Contact where lastname='world'];
        Registration__c objReg = new Registration__c(Contact__c =con.id , Event__c = eve.id , Session__C = sess.id );
        system.debug('Before-->' + sess.Total_Registrations__c);
        test.startTest();
        insert objReg;
        test.stopTest();
        Session__c resultSess = [Select name , Total_Registrations__C from Session__C where name='Session1'];
        system.debug('After-->' + resultSess.Total_Registrations__c);
        system.assertEquals(1, resultSess.Total_Registrations__c);
    }
    static testMethod void testAfterDelete(){
        Event__c eve = [Select name from Event__c where name='testEvent1'];
        Session__c sess = [Select name , Total_Registrations__C from Session__C where name='Session1'];
        Contact con = [Select name from Contact where lastname='world'];
        Registration__c objReg = new Registration__c(Contact__c =con.id , Event__c = eve.id , Session__C = sess.id );
         Registration__c objNewReg = new Registration__c(Contact__c =con.id , Event__c = eve.id , Session__C = sess.id );
        system.debug('Before-->' + sess.Total_Registrations__c);
        test.startTest();
        insert objReg;
        insert objNewReg;
        delete objNewReg;
        test.stopTest();
        Session__c resultSess = [Select name , Total_Registrations__C from Session__C where name='Session1'];
        system.debug('After-->' + resultSess.Total_Registrations__c);
        system.assertEquals(1, resultSess.Total_Registrations__c);
    }
    
    static testMethod void testAfterUpdate(){
        Event__c eve = [Select name from Event__c where name='testEvent1'];
        Session__c sess = [Select name , Total_Registrations__C from Session__C where name='Session1'];
     	 Session__c sess1 = [Select name , Total_Registrations__C from Session__C where name='Session2'];
    	Contact con = [Select name from Contact where lastname='world'];
        Registration__c objReg = new Registration__c(Contact__c =con.id , Event__c = eve.id , Session__C = sess.id );
        insert objReg ;
        objReg.Session__C = sess1.id ; 
        system.debug('Before-->' + sess.Total_Registrations__c);
        test.startTest();
        update objReg;
        test.stopTest();
        Session__c resultSess = [Select name , Total_Registrations__C from Session__C where name='Session2'];
        system.debug('After-->' + resultSess.Total_Registrations__c);
        system.assertEquals(1, resultSess.Total_Registrations__c);
    }
    
}