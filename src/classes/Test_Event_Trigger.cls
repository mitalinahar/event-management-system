@isTest
public class Test_Event_Trigger {

    @TestSetup
    static void TestData(){
        Venue__c myVenue = new Venue__C (name='venue1' , city__C='Delhi');
        insert myVenue;
       List<Event__c> eveList = new List<Event__C>() ;
        eveList.add(new Event__C (name='testEvent1',Event_Date__C=Date.Today().addDays(3),Registration_Deadline__c=Date.Today().addDays(1),Total_Capacity__c=50,Venue__c=myVenue.id));
        eveList.add(new Event__C (name='testEvent2',Event_Date__C=Date.Today().addDays(4),Registration_Deadline__c=Date.Today().addDays(2),Total_Capacity__c=50,Venue__c=myVenue.id));
         eveList.add(new Event__C (name='testEvent3',Event_Date__C=Date.Today().addDays(5),Registration_Deadline__c=Date.Today().addDays(3),Total_Capacity__c=50,Venue__c=myVenue.id));
   		 insert eveList;
    }
    
    static testMethod void testBeforeInsert(){
        Venue__c myVenue = [Select id from Venue__C where name='venue1'];
       Event__c objEve = new Event__C (name='testEvent4',Event_Date__C=Date.Today().addDays(6),Registration_Deadline__c=Date.Today().addDays(4),Total_Capacity__c=50,Venue__c=myVenue.id);
      
        test.StartTest();
        insert objEve;
        test.StopTest();
        
        List<Event__c> listEve = [Select Id from Event__C ];
        System.assertEquals(4, listEve.size());
        
    }
    static testMethod void testBeforeUpdate(){
        Event__c objEvent = [Select id , name from Event__c where name='testEvent1'];
        objEvent.Event_Date__c = Date.Today().addDays(3);
        
         test.StartTest();
        update objEvent;
        test.StopTest();
        
         List<Event__c> listEve = [Select Id from Event__C ];
        System.assertEquals(3, listEve.size());
        
    }
    static testMethod void testAfterUpdate(){
        
        Date myDate = Date.today().addDays(3);
        integer year = myDate.year();
        integer month = myDate.month();
        integer day = myDate.day();
        DateTime startT = datetime.newInstance(year,month,day,10,00,00);
         DateTime endT = datetime.newInstance(year,month,day,11,00,00);
        
        Event__c objEvent = [Select id ,Event_Date__C , name from Event__c where name='testEvent1'];
        Session__c objSession =new Session__c(name='Session1', Event__c = objEvent.id , capacity__c=10 , Start_Time__c=startT,End_Time__c=endT);
        insert objSession;
        objEvent.Event_Date__c = Date.Today().addDays(6);
        
         test.StartTest();
        update objEvent;
        test.StopTest();
        
         List<Event__c> listEve = [Select Id from Event__C ];
        System.assertEquals(3, listEve.size());
    }
}