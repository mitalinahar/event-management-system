trigger Registration_Trigger on Registration__c (after insert , after delete , after update) {
    
    List<ID> sessionId = new List<Id>();
    List<ID> newSessionId = new List<Id>();
    List<Session__c> updateSessionList = new List<Session__c>();
    
    if(trigger.isInsert){
        for(Registration__c objReg : Trigger.new){
            sessionId.add(objReg.Session__c);
        }
    }
    if(trigger.isDelete){
        for(Registration__c objReg : Trigger.old){
            sessionId.add(objReg.Session__c);
        }
    }
    if(trigger.isUpdate){
        for(Registration__c objReg : Trigger.old){
            if(objReg.Cancel_Registration__c == false && objReg.Cancel_Registration__c != trigger.newMap.get(objReg.id).Cancel_Registration__c){
                sessionId.add(objReg.Session__C); 
            }
            else {
                sessionId.add(objReg.Session__C);
                newSessionId.add(trigger.newMap.get(objReg.id).Session__C) ;
                
            }
        }
    }
 
    system.debug('List'+sessionId);
    if(!(sessionId.isEmpty())){
        for(Session__c objSession : [Select id, Total_Registrations__c from Session__c where Id In : sessionId]){
            if(objSession.Total_Registrations__c == null){
                objSession.Total_Registrations__c = 0;
            } 
            if(trigger.isInsert){
                objSession.Total_Registrations__c = objSession.Total_Registrations__c + 1 ;
            }
            if(trigger.isDelete || trigger.isUpdate){
                objSession.Total_Registrations__c = objSession.Total_Registrations__c - 1 ;
            }
           
            updateSessionList.add(objSession);
        }
    }
    
    if(!(newSessionId.isEmpty())){
        for(Session__c objSession : [Select id , Total_Registrations__c from Session__c where Id In : newSessionId]){
            if(objSession.Total_Registrations__c == null){
                objSession.Total_Registrations__c = 0;
            }
            objSession.Total_Registrations__c = objSession.Total_Registrations__c + 1 ;
            updateSessionList.add(objSession);
        }
    }
  
    system.debug('updatedlist '+updateSessionList);
    update updateSessionList;
}