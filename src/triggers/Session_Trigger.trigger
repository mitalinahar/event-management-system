trigger Session_Trigger on Session__c (before insert , before update ) {
    
    Set<Id> myIdEvent = new Set<Id>();
    Set<Id> eventIds = new Set<Id>();
    Map<String,List<Session__C>> mapSession = new Map<String,List<Session__C>>();
    
    for(Session__c s : Trigger.new){
        String key = s.Event__c + '-' + s.Start_Time__c.time() + '-' + s.End_Time__c.time();
        if(mapSession.containsKey(key)){
            List<Session__C> lstSession = mapSession.get(key);
            lstSession.add(s);
            mapSession.put(key,lstSession);
        }
        else{
            List<Session__C> lstSession = new List<Session__C>();
            lstSession.add(s);
            mapSession.put(key,lstSession);
        }
        
    }
    system.debug('-->'+mapSession);
    for(Session__c objSession : Trigger.new){
        if(Trigger.isInsert){
            String key = objSession.Event__c + '-' + objSession.Start_Time__c.time() + '-' + objSession.End_Time__c.time();
            if(mapSession.containsKey(key)) {
                integer count = mapSession.get(key).size();
                if(count > 1)
                    objSession.addError('Cant insert object. Unique time needed'); 
            }
        }
    }
    
    for(Session__C objSession : Trigger.new){
        if(trigger.isInsert){
            eventIds.add(objSession.Event__c);
            myIdEvent.add(objSession.Event__c);
        }
        if(trigger.isUpdate){
            if(objSession.Capacity__c != trigger.oldMap.get(objSession.Id).Capacity__c &&  objSession.Capacity__c >  trigger.oldMap.get(objSession.Id).Capacity__c){ 
                eventIds.add(objSession.Event__c); 
            }
            if(trigger.oldMap.get(objSession.id).Start_Time__c != objSession.Start_Time__c || trigger.oldMap.get(objSession.id).End_Time__c != objSession.End_Time__c){
                myIdEvent.add(objSession.Event__c);
            } 
        }   
    } 
    if(!eventIds.isEmpty()){
        Map<Id,Event__C> eventMap = new Map<Id,Event__c>([Select Id ,Total_Capacity__c ,Used_Capacity__c from Event__C where id in : eventIds  ]); 
        for(Session__c objSession : Trigger.new){
            if(eventMap.containsKey(objSession.Event__c)){
                Event__c objEvent = eventMap.get(objSession.Event__c);
                if((objEvent.Total_Capacity__c - objEvent.Used_Capacity__c) < objSession.Capacity__c){
                    objSession.addError('Capacity Limit Excedded here. The remaining capacity for this event is ' + (objEvent.Total_Capacity__c - objEvent.Used_Capacity__c));
                }
            } 
        }
    }
    
    if (!myIdEvent.isEmpty()) {
        system.debug('list'+myIdEvent);
        //system.debug('NewMap'+Trigger.newMap.keyset());
        Map<Id,List<Session__c>> sessionMap = new Map<Id,List<Session__C>> ();
        for (Session__c objSession : [Select Id , Event__C, Start_Time__C , End_Time__c from Session__C where Event__C In : myIdEvent]) {
            if(sessionMap.containsKey(objSession.Event__C)) {
                List<Session__c> sessionList = sessionMap.get(objSession.Event__C);
                sessionList.add(objSession);
                sessionMap.put(objSession.Event__C, sessionList);
                
            } else {
                List<Session__c> sessionList = new List<Session__c>();
                sessionList.add(objSession);
                sessionMap.put(objSession.Event__C, sessionList);
            }
        }
        
        system.debug('myMap'+ sessionMap);     
        for(Session__c objNewSession : Trigger.new){
            if(sessionMap.containsKey(objNewSession.event__c)){
                for(Session__C 	objOldSession : sessionMap.get(objNewSession.event__c)){
                    if(objNewSession.id != objOldSession.id){
                        if(objNewSession.Start_Time__c == objOldSession.Start_Time__c && objNewSession.End_Time__c ==  objOldSession.End_Time__c)
                            objNewSession.addError('Session Start Time already Exists. Choose a different Time');
                        else if(objNewSession.End_Time__c > objOldSession.Start_Time__c &&  objNewSession.End_Time__c <=  objOldSession.End_Time__c)
                            objNewSession.addError('Session End Time already Exists. Choose a different Time');
                        else if(objNewSession.Start_Time__c >= objOldSession.Start_Time__c && objNewSession.Start_Time__c <  objOldSession.End_Time__c)
                            objNewSession.addError('Session Time already Exists. Choose a different Time');
                    } 
                }
            }
        }
        
    }
    
}