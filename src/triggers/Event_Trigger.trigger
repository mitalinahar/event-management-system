trigger Event_Trigger on Event__c (before insert , before update , after update) {
    
    set<Date> dateSet = new Set<Date>();
    for(Event__C objEvent : trigger.new){
        dateSet.add(objEvent.Event_Date__c); 
    } 
    system.debug('dateSet'+dateSet);
    
    List<Event__C> eveList = [Select Event_Date__C from Event__C where Event_Date__c in : dateSet];
    Map<Date,Event__C> eventMap = new Map<Date,Event__C> (); 
    for(Event__c eveObj : eveList){
        if(!eventMap.containsKey(eveObj.Event_Date__C))
            eventMap.put(eveObj.Event_Date__C,eveObj);
    }
    
    for(Event__c objEvent : trigger.new){
        if(trigger.isInsert){
            if(eventMap.containsKey(objEvent.Event_Date__c))
                objEvent.addError('There is already an event on this date. Please choose a different Date');
        }
        
        else if(trigger.isUpdate && trigger.isBefore ){
            if(objEvent.Event_Date__c != trigger.oldMap.get(objEvent.id).Event_Date__C){
                if(eventMap.containsKey(objEvent.Event_Date__c) && objEvent.id != eventMap.get(objEvent.Event_Date__c).id )
                    objEvent.addError('There is already an event on this date. Please choose different Date');  
            }
        }
    }
    
    if(trigger.isUpdate && trigger.isAfter) //trigger is after update
    {   
        List<Id> eventIds = new List<Id>();
        List<Session__c> updateList = new List<Session__C>();
        for(Event__C objEve : trigger.new){
            If(objEve.Event_Date__C != trigger.oldmap.get(objEve.Id).event_Date__c){
                eventIds.add(objEve.Id);
            }
        }
        System.debug('List of Ids'+eventIds);
        List<Event__c> lstEvent = new List<Event__C>([Select Id , Event_Date__c ,(Select Id, Start_Time__c , End_Time__c from Sessions__r) from Event__C where id in : eventIds]);
        system.debug('List of Events'+lstEvent);
        
        for(Event__c newEve : lstEvent){
            system.debug('Inside after update');
            for(Session__c objSession : newEve.Sessions__r){
                system.debug('Inside list');
                Date newDate = Date.valueOf(newEve.Event_Date__c);
                Datetime oldStartValue = Datetime.valueOf(objSession.Start_Time__c);
                Integer startHour = oldStartValue.hour();
                Integer startMinute = oldStartValue.minute();
                Time oldStartTime = Time.newInstance(startHour, startMinute, 0 , 0);
                DateTime newStartValue = DateTime.newInstance(newDate, oldStartTime);
                objSession.Start_Time__c = newStartValue;
                
                Datetime oldEndValue = Datetime.valueOf(objSession.End_Time__c);
                Integer EndHour = oldEndValue.hour();
                Integer EndMinute = oldEndValue.minute();
                Time oldEndTime = Time.newInstance(EndHour, EndMinute, 0 , 0);
                DateTime newEndValue = DateTime.newInstance(newDate, oldEndTime);
                objSession.End_Time__c = newEndValue;
                
                updateList.add(objSession);  
            } 
        }  
        update updateList;
    } 
}